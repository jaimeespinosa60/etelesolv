﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtelesolvModel
{
    /// <summary>
    /// Holds category information for the products.
    /// </summary>
    public class productCategory
    {
        private double _taxRate;

        // constructors
        public productCategory()
        { }
        public productCategory(int ID, string name, double rate)
        {
            this.ID = ID;
            this.name = name;
            this.taxRate = rate;
        }


        public int ID { get; set; }
        public string name { get; set; }

        /// <summary>
        /// Can not be null. must be above zero and below or equal 100
        /// I suppose the value can change in the future only for one category. 
        /// </summary>
        public double taxRate
        {
            get { return _taxRate; }
            set
            {
                if (value < 0 | value > 100)
                    throw new Exception("Value can not be less than zero or greater than 100");

                _taxRate = value;
            }
        }
    }
}
