﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtelesolvModel
{
    public class product
    {

        // constructor
        public product()
        { }

        public product(int ID, int productCategoryID, string name, double price, bool isImported = false)
        {
            this.ID = ID;
            this.productCategoryID = productCategoryID;
            this.name = name;
            this.price = price;
            this.isImported = isImported;
        }

        public int ID { get; set; }

        public int productCategoryID { get; set; }

        public string name { get; set; }

        public double price { get; set; }

        public bool isImported { get; set; }

    }
}
