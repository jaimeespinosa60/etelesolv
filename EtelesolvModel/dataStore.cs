﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EtelesolvModel
{
    /// <summary>
    /// simulates a database
    /// </summary>
    public class dataStore
    {
        public double importedProductsTaxRate = 5; // percent

        // categories list
        Dictionary<int, EtelesolvModel.productCategory> _categories;

        // products list
        Dictionary<int, EtelesolvModel.product> _products;

        public Dictionary<int, EtelesolvModel.productCategory> categories
        {
            get
            { return _categories; }
        }

        public Dictionary<int, EtelesolvModel.product> products
        {
            get
            { return _products; }
        }

        public dataStore()
        {
            initalizeCategories();
            initializeProducts();
        }

        private void initalizeCategories()
        {
            _categories = new Dictionary<int, EtelesolvModel.productCategory>();

            _categories.Add(1, new productCategory(1, "books", 0));
            _categories.Add(2, new productCategory(2, "food", 0));
            _categories.Add(3, new productCategory(3, "medical products", 0));
            _categories.Add(4, new productCategory(4, "music", 10));
            _categories.Add(5, new productCategory(5, "perfumes", 10));
        }

        private void initializeProducts()
        {
            _products = new Dictionary<int, product>();

            _products.Add(1, new product(1, 1, "book", 12.49));
            _products.Add(2, new product(2, 4, "music cd", 14.99));
            _products.Add(3, new product(3, 2, "chocolate bar", 0.85));

            _products.Add(4, new product(4, 2, "imported box of chocolates", 10.00, true));
            _products.Add(5, new product(5, 5, "imported bottle of perfume", 47.50, true));

            _products.Add(6, new product(6, 5, "bottle of perfume", 18.99));
            _products.Add(7, new product(7, 3, "packet of headache pills", 9.75));

            _products.Add(8, new product(8, 5, "imported bottle of perfume 2", 27.99, true));
            _products.Add(9, new product(9, 2, "imported box of chocolates 2", 11.25, true));
        }

        public productCategory categoryByID(int categoryID)
        {
            if (_categories.Keys.Contains(categoryID))
                return _categories[categoryID];
            else
                throw new Exception("product category not found");
        }
        public product productByID(int productID)
        {
            if (_products.Keys.Contains(productID))
                return _products[productID];
            else
                throw new Exception("product not found");
        }
    }
}
