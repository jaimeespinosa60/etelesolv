﻿using EtelesolvModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etelesolv
{
    class Program
    {
        private static basket testBasket;
        static void Main(string[] args)
        {
            string option = "";
            int optionValue = 0;

            while (optionValue != -1)
            {
                Console.Clear();
                Console.WriteLine("Please select one option:");

                // the requiered by the test
                Console.WriteLine("1. Test case 1");
                Console.WriteLine("2. Test case 2");
                Console.WriteLine("3. Test case 3");

                // an emtpy one to try
                Console.WriteLine("4. Your own one");
                Console.WriteLine("Q. Quit");

                optionValue = 0;
                option = Console.ReadLine();

                // checks for quit option
                if (option.ToLower().Trim() == "q")
                {
                    Console.WriteLine("Ending. Press enter");
                    Console.ReadLine();
                    Environment.Exit(0);
                }

                int.TryParse(option, out optionValue);

                switch (optionValue)
                {
                    case 1:
                        testCase1();
                        break;
                    case 2:
                        testCase2();
                        break;
                    case 3:
                        testCase3();
                        break;
                    case 4:
                        yourOwnOne();
                        break;

                    default:
                        break;
                }
            }
        }
        private static void testCase1()
        {
            // inits basket
            testBasket = new basket();

            // adds products
            // category IDs should come from previous product selection operation
            testBasket.add(1, 1);
            testBasket.add(1, 2);
            testBasket.add(1, 3);

            Console.Write(testBasket.printReceipt());
            Console.WriteLine();
            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
        }
        private static void testCase2()
        {
            // inits basket
            testBasket = new basket();

            // adds products
            // category IDs should come from previous product selection operation
            testBasket.add(1, 4);
            testBasket.add(1, 5);

            Console.Write(testBasket.printReceipt());
            Console.WriteLine();
            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
        }
        private static void testCase3()
        {
            // inits basket
            testBasket = new basket();

            // adds products
            // category IDs should come from previous product selection operation
            testBasket.add(1, 8);
            testBasket.add(1, 6);
            testBasket.add(1, 7);
            testBasket.add(1, 9);

            Console.Write(testBasket.printReceipt());
            Console.WriteLine();
            Console.WriteLine("Press enter to continue");
            Console.ReadLine();
        }
        private static void yourOwnOne()
        {
            // inits basket
            testBasket = new basket();

            string option = "";
            int optionValue = 0;

            while (optionValue != -1)
            {
                Console.Clear();
                Console.WriteLine("Please add products to your cart by selecting a number");

                foreach (KeyValuePair<int, product> prod in testBasket.productDatabase.products)
                    Console.WriteLine(string.Format("{0}\t{1}\t{2}", prod.Value.ID,
                                                                     prod.Value.name.PadRight(30, ' '),
                                                                     testBasket.productDatabase.categoryByID(prod.Value.productCategoryID).name).PadLeft(30, ' '));

                Console.WriteLine("P. Print receipt");
                Console.WriteLine("C. Cancel");

                optionValue = 0;

                option = Console.ReadLine();

                // checks for quit option
                if (option.ToLower().Trim() == "c")
                {
                    optionValue = -1;
                    continue;
                }

                if (option.ToLower().Trim() == "p")
                {
                    Console.Write(testBasket.printReceipt());
                    Console.WriteLine();
                    Console.WriteLine("Press enter to continue");
                    Console.ReadLine();

                    optionValue = -1;
                    continue;
                }

                int.TryParse(option, out optionValue);

                product productAdd = testBasket.productDatabase.productByID(optionValue);
                if (productAdd != null)
                {
                    int quantity = 0;
                    Console.WriteLine("How many ?:");
                    int.TryParse(Console.ReadLine(), out quantity);

                    if (quantity > 0)
                        testBasket.add(quantity, productAdd.ID);
                }
            }
        }
    }
}
