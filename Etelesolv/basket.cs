﻿using EtelesolvModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Etelesolv
{
    public class basket
    {
        // the basket
        private List<KeyValuePair<int, EtelesolvModel.product>> _basketProducts;

        // datastore (simulated database)

        private dataStore _productDatabase;

        // the receipt (for printing)
        private StringBuilder outputReceipt;

        public dataStore productDatabase
        {
            get
            { return _productDatabase; }
        }

        public basket()
        {
            _basketProducts = new List<KeyValuePair<int, EtelesolvModel.product>>();
            _productDatabase = new dataStore();
        }

        public void add(int quantity, int productID)
        {
            _basketProducts.Add(new KeyValuePair<int, EtelesolvModel.product>(quantity, _productDatabase.productByID(productID)));
        }

        public string printReceipt()
        {
            double total = 0.0;
            double totalTax = 0.0;

            outputReceipt = new StringBuilder("Some header");
            outputReceipt.Append(Environment.NewLine);

            foreach (KeyValuePair<int, product> basketItem in _basketProducts)
            {
                // gets tax rate
                double taxRate = _productDatabase.categoryByID(basketItem.Value.productCategoryID).taxRate;

                // gets imported product tax rate if applies
                double importedGoodsTaxRate = basketItem.Value.isImported ? _productDatabase.importedProductsTaxRate : 0;

                // price X quantity
                double itemsPrice = basketItem.Value.price * basketItem.Key;

                // calculates taxes
                double itemsTaxes = Math.Round(itemsPrice * (taxRate + importedGoodsTaxRate) / 5, MidpointRounding.AwayFromZero) / 20;

                total += itemsPrice;
                totalTax += itemsTaxes;

                outputReceipt.Append(string.Format("{0} {1}\t{2:#00.00}", basketItem.Key,
                                                                      basketItem.Value.name.PadLeft(30, ' '),
                                                                      itemsPrice + itemsTaxes));
                outputReceipt.Append(Environment.NewLine);

            }

            outputReceipt.Append(Environment.NewLine);
            outputReceipt.Append(string.Format("Sales taxes\t{0:#00.00 USD}", totalTax)); outputReceipt.Append(Environment.NewLine);
            outputReceipt.Append(string.Format("Total\t\t{0:#00.00 USD}", total + totalTax)); outputReceipt.Append(Environment.NewLine);

            outputReceipt.Append(Environment.NewLine);


            return outputReceipt.ToString();
        }


    }
}
